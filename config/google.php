<?php
return [
    'sheets' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'app_name' => env('GOOGLE_APP_NAME'),
        'redirect_url' => env('GOOGLE_REDIRECT_URL'),
    ]
];
