<?php

namespace App\Http\Controllers\Google;

use App\Helpers\Google\GoogleClientFactory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OauthController extends Controller
{
    /**
     * Метод, который висит на redirectUrl Google авторизации
     * @param Request $request
     * @return false|\Google\Service\Drive\AboutStorageQuota|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|string
     */
    public static function oauth(Request $request)
    {
        if (!$request->exists("code")) {
            return redirect(GoogleClientFactory::getOauthInstance(true)->createAuthUrl());
        }

        $client = GoogleClientFactory::getOauthInstance(true);
        $token = $client->fetchAccessTokenWithAuthCode($request->get("code"));


        if (isset($token["error"])) {
            $str_token = json_encode($token);
            return inertia("Google/Show", ["message" => "Ошибка получения токена: {$str_token}"]);
        }

        Storage::disk("local")->put("google/token.json", json_encode($token));

        return inertia("Google/Show", ["message" => "Вы успешно авторизовались в google!"]);;
    }
}
