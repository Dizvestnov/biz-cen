<?php

use App\Http\Controllers\ClientFormController;
use App\Http\Controllers\Google\OauthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [ClientFormController::class, "index"])->name('index');
Route::post("form/submit", [ClientFormController::class, "store"])->name('form.submit');

Route::get("link/list", [ClientFormController::class, "show"])->name('link.list');

Route::get("google/oauth", [OauthController::class, "oauth"])->name('google.oauth');

/*
    Route::middleware('auth')->group(function () {
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    });
 */


// require __DIR__ . '/auth.php';
