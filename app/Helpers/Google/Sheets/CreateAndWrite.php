<?php

namespace App\Helpers\Google\Sheets;

use App\Helpers\Google\GoogleClientFactory;

class CreateAndWrite
{
    public static function writeToGoogleSheet($data)
    {
        $client = GoogleClientFactory::getOauthInstance();

        $sheetsService = new \Google_Service_Sheets($client);

        // Создание новой таблицы
        $spreadsheetProperties = new \Google_Service_Sheets_SpreadsheetProperties();
        $spreadsheetProperties->setTitle($data['title']);
        $spreadsheet = new \Google_Service_Sheets_Spreadsheet(['properties' => $spreadsheetProperties]);
        $spreadsheet = $sheetsService->spreadsheets->create($spreadsheet);

        // Запись данных в таблицу
        $values = $data['values'];
        $body = new \Google_Service_Sheets_ValueRange(['values' => $values]);
        $sheetsService->spreadsheets_values->update($spreadsheet->spreadsheetId, 'A1', $body, ['valueInputOption' => 'RAW']);

        // Получение ссылки на таблицу
        $spreadsheetUrl = "https://docs.google.com/spreadsheets/d/{$spreadsheet->spreadsheetId}";

        return $spreadsheetUrl;
    }
}
