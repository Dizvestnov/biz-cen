<?php

namespace App\Http\Controllers;

use App\Helpers\Google\Sheets\CreateAndWrite;
use App\Http\Requests\StoreFormRequest;
use App\Http\Resources\GoogleSheetsLinkResource;
use App\Models\GoogleSheetsLink;
use Illuminate\Support\Facades\Storage;

class ClientFormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Если нет авторизации в гугл то идем авторизоваться
        if (!Storage::disk("local")->exists("google/token.json")) {
            // Если нет активной авторизации, делаем редирект на маршрут авторизации Google
            return to_route('google.oauth');
        }

        return inertia('Welcome');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFormRequest $request)
    {
        $spreadsheetUrl = CreateAndWrite::writeToGoogleSheet($request);
        // Сохранение ссылки в базу данных
        $googleSheetLink = new GoogleSheetsLink();
        $googleSheetLink->url = $spreadsheetUrl;
        $googleSheetLink->save();



        return to_route('link.list');
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $links = GoogleSheetsLink::all();

        // Преобразуем коллекцию моделей в ресурсные объекты
        $linksResource = GoogleSheetsLinkResource::collection($links);

        return inertia('Google/Links/List', ['links' => $linksResource]);
    }
}
