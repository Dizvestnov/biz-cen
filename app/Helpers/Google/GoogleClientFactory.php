<?php

namespace App\Helpers\Google;

use Google\Cloud\Core\Exception\GoogleException;
use Google_Client;
use Google_Service_Drive;
use Illuminate\Support\Facades\Storage;


class GoogleClientFactory
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws GoogleException
     */
    public static function getOauthInstance($withoutAuth = false)
    {
        $client = new \Google_Client();

        $client->setClientId(config('google.sheets.client_id'));
        $client->setClientSecret(config('google.sheets.client_secret'));
        $client->setApplicationName(config('google.sheets.app_name'));
        $client->setRedirectUri(config('google.sheets.redirect_url'));
        $client->addScope(Google_Service_Drive::DRIVE);

        $client->setAccessType("offline");
        $client->setPrompt("consent");

        if ($withoutAuth) return $client;

        if (!Storage::disk("local")->exists("google/token.json")) {
            throw new GoogleException("Отсутствует токен для авторизации");
        }

        $token = json_decode(Storage::disk("local")->get("google/token.json"), true);
        $client->setAccessToken($token);

        if ($client->isAccessTokenExpired()) {
            $token = $client->fetchAccessTokenWithRefreshToken();
            Storage::disk("local")->put("google/token.json", json_encode($token));
        }

        return $client;
    }
}
